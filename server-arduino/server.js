const SerialPort  = require('serialport');
const Readline    = require('@serialport/parser-readline');
const port        = new SerialPort('/dev/ttyACM0',  {baudRate: 9600});
const parser      = port.pipe(new Readline({ delimiter: '\n' }));
const express        = require('express');

const cors = require('cors')

// const animations  = require('./animations');


function andar(num) {
    let sendData = [ num ];

    port.write(sendData, (err) => {
        if (err) {
            console.log(err);
        }
    });

}

const app = express();

app.options('*', cors()) // include before other routes

app.use(cors({
    origin: "http://localhost:4200", // restrict calls to those this address
    methods: "GET" // only allow GET requests
}))

app.use(express.json());


app.post('/andar', function ( req, res ){

   console.log(req.body);

   andar(req.body.andar);
   

   res.status(200).json({ message: 'ok' });
});

app.listen(3001, ()=>{
    console.log("server listening to 3001");
});


port.on("open", () => {
    console.log('serial port open');
});


/**
 * Recebimento de dados do arduino
 */
parser.on('data', d => {
    console.log('data', d);
    // const data = JSON.parse(d);
    // console.log('[Serial port] data received', data);

});

