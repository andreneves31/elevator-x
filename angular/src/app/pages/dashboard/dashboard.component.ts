import { Component, ChangeDetectorRef } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
    readonly maxIntensityGraphicConstant = 2;
    readonly minIntensityGraphicConstant = 1.1;

    readonly buttonSize : number = 120;
    

    currentIntensityValue: number = 0;
    currentIntensityProportion: number = 0;
    
    readonly defaultMaxIntensity = 70;

    minIntensity: number = 0;
    maxIntensity: number = this.defaultMaxIntensity;
    
    intensityGraphicSize : number = this.buttonSize * 2;

    lastUpdatedIntensity = new Date();
    lastUpdatedMaxIntensity = new Date();

    buttonTopOffset : number;
    buttonLeftOffset : number;


    readonly triggerIntensity = 30;


    /**
     * SpeechRecognition
     */
    recognition : SpeechRecognition;
    speechRecognitionList : SpeechGrammarList;


    recognizing : boolean = true;
    canListen : boolean = true;


    defaultTextMsg = 'Para onde deseja ir?';
    textMsg = this.defaultTextMsg;
    feedBackMsg = '';

    countDownInterval;
    countDownTimeRemaining;


    clearFeedbackMsgTimeout;

    synth = window.speechSynthesis;

    
    constructor(
        private breakpointObserver: BreakpointObserver,
        private changeDetecton : ChangeDetectorRef   ,
        private http : HttpClient 
    ) {
        this.observeIntensity();
        this.setGraphicSize();
        this.configureSpeechRecognition();
        this.testProcessing();

    }


    observeIntensity() {

        navigator.mediaDevices.getUserMedia({
            audio: true
        }).then(stream => {
            
            const audioContext = new AudioContext();
            const input = audioContext.createMediaStreamSource(stream);
            const analyser = audioContext.createAnalyser();
            const scriptProcessor = audioContext.createScriptProcessor();


            analyser.smoothingTimeConstant = 0.3;
            analyser.fftSize = 1024;
            
            input.connect(analyser);
            analyser.connect(scriptProcessor);
            scriptProcessor.connect(audioContext.destination);

            const processInput = audioProcessingEvent => {
                const tempArray = new Uint8Array(analyser.frequencyBinCount);
                analyser.getByteFrequencyData(tempArray);

                const intensity = this.getAverageVolume(tempArray);
                this.setIntensity(intensity);

            }


            scriptProcessor.onaudioprocess = processInput;

        }, error => {
            console.error(error);
        });


    }

    setIntensity(intensity) {
        this.currentIntensityValue = intensity;
        

        if(intensity > this.maxIntensity || this.maxIntensity == undefined) {
            this.maxIntensity = intensity;
            this.lastUpdatedMaxIntensity = new Date();
        } else if( (new Date().getTime() - this.lastUpdatedMaxIntensity.getTime()) > 10000) {
            this.maxIntensity = this.defaultMaxIntensity;
        }

        this.currentIntensityProportion = 
            (this.currentIntensityValue - this.minIntensity) / 
            (this.maxIntensity - this.minIntensity);
        

        this.setGraphicSize();

        if(this.currentIntensityValue >= this.triggerIntensity && !this.synth.speaking ) {
            this.initRecognition();
        }
    }

    configureSpeechRecognition() {

        var SpeechRecognition = eval('SpeechRecognition || webkitSpeechRecognition');
        var SpeechGrammarList = eval('SpeechGrammarList || webkitSpeechGrammarList');


            
        this.recognition = new SpeechRecognition();
        this.speechRecognitionList = new SpeechGrammarList();


        console.log(this.getDominios().reduce((acc, cur) => {
            const d = cur.variantes.reduce((acc, cur) => {
                const d = cur.acionadores.reduce((acc, cur) => {
                    acc.push(cur);

                    return acc;
                }, []);

                acc.push(d);
                
                return acc;
            }, []);

            acc.push(d);
            
            return acc;
        }, []));

        var words = [
            'primeiro',
            'segundo',
            'terceiro',
            'quarto',

            'um',
            'dois',
            'três',
            'quatro',

            'apartamento',
            'José',
            'Carlos',
            'Renata',
            'Luiza',
        ];

        var grammar = '#JSGF V1.0; grammar words; public <word> = ' + words.join(' | ') + ' ;'

        this.speechRecognitionList.addFromString(grammar, 1);
        this.recognition.grammars = this.speechRecognitionList;
        
        this.recognition.continuous = true;
        this.recognition.lang = 'pt-BR';

        this.recognition.interimResults = false;
        this.recognition.maxAlternatives = 1;

        this.recognizing = false;
        
        // this.initRecognition();


        this.recognition.onresult = (event : SpeechRecognitionEvent) => {
            this.resultRecognition(event);
            
        }

        this.recognition.onend = () => {
            this.endRecognition();
        }

        this.recognition.onspeechend = () => {
            this.recognition.stop();
            console.log('Speech end');

        }

        this.recognition.onnomatch = (event) => {
            console.log('no match');
        }

        this.recognition.onerror = (event) => {
            console.log('error');
        }
    }

    setGraphicSize() {
        const intensityGraphicProportion = (
            this.minIntensityGraphicConstant + (
                (this.maxIntensityGraphicConstant - this.minIntensityGraphicConstant) * 
                this.currentIntensityProportion
            )
        );

        this.intensityGraphicSize = 
            this.buttonSize * intensityGraphicProportion;
            // this.buttonSize * 1.5;



        this.buttonLeftOffset = (
            this.intensityGraphicSize - this.buttonSize
        ) / 2;

        this.buttonTopOffset = -this.intensityGraphicSize + (
            this.intensityGraphicSize - this.buttonSize
        ) / 2;
                
                
        let diff = new Date().getTime() - this.lastUpdatedIntensity.getTime();

        if(diff > 20) {
            this.changeDetecton.detectChanges();
            this.lastUpdatedIntensity = new Date();
        }
    }

    getAverageVolume(array) {
        const length = array.length;
        let values = 0;
        let i = 0;

        for (; i < length; i++) {
            values += array[i];
        }

        return values / length;
    }




    initRecognition() {
        // this.recognition.start();

        if(this.recognizing)
            return;


        console.log('Esperando por comando de voz para iniciar...');
        this.recognition.stop();
        this.recognition.start();
        console.log('Diga algo...');

        this.countDownTimeRemaining = 5;

        this.textMsg = `Fale agora: 0:0${this.countDownTimeRemaining}`;

        clearInterval(this.countDownInterval);

        this.countDownInterval = setInterval(() => {
            this.countDownTimeRemaining--;

            if(this.countDownTimeRemaining < 0)
                this.countDownTimeRemaining = 0;

            this.textMsg = `Fale agora: 0:0${this.countDownTimeRemaining}`;

            console.log('countdown');
        }, 1000);

        this.recognizing = true;
        
        
        setTimeout(() => { 
            if(!this.recognizing)
                return;
            
            this.recognition.stop();
            this.recognizing = false;
            console.log('Stopped');
            this.textMsg = this.defaultTextMsg;
        }, 5000);
    }

    

    endRecognition() {
        console.log('end.....');

        this.recognizing = false;
        this.recognition.stop();

        clearInterval(this.countDownInterval);
        this.textMsg = this.defaultTextMsg

    }

    resultRecognition(event : SpeechRecognitionEvent) {
        this.endRecognition();
        
        // console.log(event);

        var last = event.results.length - 1;
        var result = event.results[last][0].transcript;

        console.log(result);

        // console.log('Result received: ' + result + '.');
        // console.log('Confidence: ' + event.results[0][0].confidence);

        this.processResult(result);
    }


    testProcessing() {
        // this.processResult('Vá para o PRIMEIRO andar');
        // this.processResult('Vá para o último andar');
        

        // setTimeout(() => this.processResult('Vá para o PRIMEIRO andar'), 5000)
        // this.processResult('Vá para o sexto andar');
    }


    processResult(result) {
        console.log(result);

        const correspondencias = this.getCorrespondencias(result);
        const comandoAndar = correspondencias.filter(
            (item) => item.dominio.comando === 'ir-para-andar'
        )[0];

        if(comandoAndar != undefined) {
            const variante = comandoAndar.variantesCorrenpondidas[0];
            this.comandoIrParaAndar(variante.arg);
        } else {
            this.comandoNaoReconhecido();
        }
    }

    comandoIrParaAndar(andar) {
        if(andar == 0)
            this.sendFeedbackMsg('Indo para o térreo');
        else
            this.sendFeedbackMsg(`Indo para o ${andar}º andar`);

        console.log('andar', 1);

        this.http.post(
            'http://localhost:3001/andar', 
            {andar: andar}
        ).subscribe((value) => {
            console.log(value);
        });


    }

    comandoNaoReconhecido() {
        this.sendFeedbackMsg('Comando não reconhecido', false);
    }

    sendFeedbackMsg(msg, sound = true) {

        clearTimeout(this.clearFeedbackMsgTimeout);
        this.feedBackMsg = msg;


        this.clearFeedbackMsgTimeout = setTimeout(() => {
            this.feedBackMsg = '';
        }, 10000);

        if(sound)
            this.synthVoice(msg);
    }

    synthVoice(text) {
        console.log('sintetizando áudio...');

        let synthUtter = new SpeechSynthesisUtterance(text);

        synthUtter.pitch = 1;
        synthUtter.rate = 1;  
        synthUtter.voice = this.synth.getVoices().filter(function(v) {return v.lang == 'pt-BR'})[0];

        const a = this.synth.speak(synthUtter);
        
    }


    getCorrespondencias(result) {
        const dominios = this.getDominios();
        
        return dominios.reduce((acc, dominio) => {
            const variantesCorrenpondidas = dominio.variantes.reduce((acc, variante) => {

                const match = variante.acionadores.filter(
                    (acionador) => result.toLowerCase().includes(acionador)
                ).length > 0;

                
                if(match) {
                    acc.push(variante);
                }

                return acc;
            }, []);

            if(variantesCorrenpondidas.length > 0) {
                acc.push({dominio, variantesCorrenpondidas});
            }

            return acc;
        }, []);


    }

    getDominios() {
        return  [
            {
                comando: 'ir-para-andar',
                variantes: [
                    {acionadores: ['terra', 'terreo', 'garagem', 'portaria'], arg: 0},
                    {acionadores: ['primeiro', 'um', 'joão'], arg: 1},
                    {acionadores: ['segundo', 'dois', 'marcelo'], arg: 2},
                    {acionadores: ['terceiro', 'três', 'último', 'mariana', 'andré' , 'felipe', ], arg: 3}      
                ]
            }
        ];
    }
}
