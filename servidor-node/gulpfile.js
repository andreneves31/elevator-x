const gulp = require('gulp');
const clean = require('gulp-clean');
const ts = require('gulp-typescript');
const { spawn } = require('child_process');

const tsProject = ts.createProject('tsconfig.json');



gulp.task('default', ['watch']);


//Automatizar
gulp.task('watch', ['build'], () => {
    // Ativar o nodemon para monitorar mudanças
    const nodemon = spawn('node_modules/.bin/nodemon', ['--inspect', '--delay', '5', 'dist/server/index.js']);

    nodemon.stdout.on('data', (data) => {
        console.log(`${data}`);
    });

    nodemon.stderr.on('data', (data) => {
        console.log(`stderr: ${data}`);
    });

    nodemon.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });


    
    return gulp.watch(
        ['src/**/*.ts', 'src/**/*.json'],
        ['build']
    );
});

//Copiar os arquivos estáticos do diretório 'src' para 'dist
gulp.task('static', ['clean'], () => {
    return gulp
        .src(['src/**/*.json'])
        .pipe(gulp.dest('dist'));
});


//Compilação
gulp.task('scripts', ['static'], () => {
    const tsResult = tsProject.src()
        .pipe(tsProject());

    return tsResult.js
        .pipe(gulp.dest('dist'));
});


//Limpar o diretório dist
gulp.task('clean', () => {
    return gulp
        .src('dist')
        .pipe(clean());
});

//Principal
gulp.task('build', ['scripts']);
