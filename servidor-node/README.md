# Inicialização



* Instalar as dependências `npm install`

* Executar em modo de desenvolvimento: `npm run dev`


<br>


# Estrutura do código fonte

## Pastas da raíz

* **business**: Contém as implementações que referem-se apenas ao negócio específico implementado na API. Não deve ter implementações que se referem a aspectos técnicos do servidor

* **config**: Configurações do servidor, como endereços de banco de dados

* **graphql**: Implementações específicas do GraphQL

* **mongodb**: Implementações específicas do MongoDB

* **server**: Implementações específicas do entrypoint da API


<br>

# Business

<br>

# GraphQL



<br>


# Aspetos gerais da implementação

* Para log no console, deve ser usada a função `logMsg`, presente no arquivo `/server/utils/utils.ts`. Ela é apropriada para logs em caso de utilização de cluster (vários processos)



