
export const isDev = process.env.NODE_ENV === 'development'



export class ErroDesconhecidoException extends Error {
    constructor() {
        super('Erro desconhecido');
        this.name = 'ErroDesconhecidoException'
    }
}
