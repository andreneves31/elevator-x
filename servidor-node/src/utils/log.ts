import { isDev } from "./global";

export const logMsg = (...msg : any) => {
    if(isDev) {
        console.log(...msg);
    } else {
        process.send(msg);
    }
}



export const logError = (...msg : any) => {
    if(isDev) {
        console.error(...msg);
    } else {
        process.send(msg);
    }
}