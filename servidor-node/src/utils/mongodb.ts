import * as mongoose from 'mongoose';


export const getModelOptions = { 
    schemaOptions: { 
        timestamps: true,
        autoIndex: false
    }
};

export const getModelForClassFunction  = 
    (Class) => {
        let model : MongooseModel<MongooseDocument<any>, any>;
        
        model = new Class().getModelForClass(Class, getModelOptions);
        model.createIndexes();

        return model;
    }



export const verifyDocumentExists =
    async (model, id, entityName) => {
        const qt = await model.findById(id).countDocuments();
        if(qt == 0) {
            throw new DocumentNotFoundException(entityName, id);
        }
    }

export const updateDocument =
    async (model : MongooseModel<any, any>, id : string, input : any, entityName : string) : Promise<MongooseDocument<any>> => {
        await verifyDocumentExists(model, id, entityName);

        const resp = await model.updateOne({_id: id}, input);

        if(resp.ok == 1) {
            // TODO: Filtrar campos requeridos
            return await model.findById(id);
        } else {
            throw new UpdateFailException(entityName, id);
        }

    }

export const deleteDocument =
    async (model : MongooseModel<any, any>, id : string, entityName : string) : Promise<MongooseDocument<any>> => {
        const doc = await model.findById(id);
        
        if(doc == null) {
            throw new DocumentNotFoundException(entityName, id);
        }

        const resp = await model.deleteOne({_id: id});

        console.log(resp);

        if(resp.ok == 1) {
            return doc;
        } else {
            throw new DeleteFailException(entityName, id);
        }
    }

export class DocumentNotFoundException extends Error {
    constructor(entityName, id) {
        super(`Documento do tipo ${entityName}, id=(${id}) não encontrado`);
        this.name = 'DocumentNotFoundException'
    }
}


export class UpdateFailException extends Error {
    constructor(entityName, id) {
        super(`Falha ao atualizar ${entityName}, id=(${id})`);
    }
}


export class DeleteFailException extends Error {
    constructor(entityName, id) {
        super(`Falha ao deletar ${entityName}, id=(${id})`);
    }
}

export type MongooseDocument<Entity> = mongoose.Document & Entity;
export type MongooseModel<T extends MongooseDocument<Entity>, Entity> = mongoose.Model<T, {}>





   