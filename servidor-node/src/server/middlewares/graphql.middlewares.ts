import { RequestHandler, Request, Response, NextFunction } from "express";
import * as graphqlHTTP from 'express-graphql';
import schema from '../../graphql/graphqlSchema';
import { isDev } from "../../utils/global";
import { Server } from "..";

export const graphqlPrepareContextMiddleware = (instance : Server) => {
    return (req: Request, res: Response, next: NextFunction): void => {
        req['context'] = {};
        req['context'].db = instance.db;

        next();
    };
};

export const graphqlHandlerMiddleware = () => {
    return graphqlHTTP((req) => ({
        schema: schema,
        graphiql: isDev,
        context: req['context']
    }));
};