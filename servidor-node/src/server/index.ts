import  * as express from 'express';
import * as http from 'http';
import * as mongoose from 'mongoose';
import * as websocket from 'websocket';

import { normalizePort, onError, onListening } from '../utils/utils';
import { graphqlPrepareContextMiddleware, graphqlHandlerMiddleware } from './middlewares/graphql.middlewares';
import config from '../config/config';
import { ObjectId } from 'bson';
import { logError, logMsg } from '../utils/log';
import { isDev } from '../utils/global';
import * as fs from 'fs';

export class Server {
    express : express.Application;
    server : http.Server; 

    port : number;
    host : string;

    db ;


    constructor() {
        this.express = express();

        this.declareMiddlewares();
        this.initServer();



    }


    private declareMiddlewares() : void {

        this.express.use('/graphql',  
            graphqlPrepareContextMiddleware(this),
            graphqlHandlerMiddleware()            
        );


        this.express.use('/hello', (req : express.Request, res : express.Response, next : express.NextFunction) => {
            res.send({
                hello: 'Hello World!'
            });
        });


    }


    private async initServer() : Promise<void> {
        this.server = http.createServer(this.express);
        this.port  = normalizePort(process.env.port || 3000);
        this.host = process.env.host || '127.0.0.1';

        

        

        try {
            await this.initDb();
            this.serverReady();
        } catch (e) {
            logError(e);
        }

    }

    private async initDb() : Promise<void> {
        let dbConfig : {uri: string, label: string};

        if(isDev) {
            dbConfig = config.db.development;
        } else {
            dbConfig = config.db.production;
        }

        try {
            await mongoose.connect(dbConfig.uri, {useNewUrlParser: true});
            this.db = mongoose.connection;

            logMsg(`Banco de dados conectado: ${dbConfig.label}`);
        } catch(e) {
            logError('connection error:');
            logError(e);
            throw(`Falha ao conectar ao banco: ${dbConfig.label}` );
        }        
    }


    private serverReady() {
        this.server.listen(this.port, this.host);
        this.server.on('error', onError(this.server));
        this.server.on('listening', onListening(this.server));



        var websocket = require('websocket-stream')
        var wss = websocket.createServer({server: this.server}, handle)

        function handle(stream, request) {
            // `request` is the upgrade request sent by the client.
            const file = fs.createWriteStream('./teste.wav');

            stream.pipe(file);
        }
    }


}


export default new Server();

