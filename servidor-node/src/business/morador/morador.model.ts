import * as mongoose from 'mongoose';
import { prop, Typegoose, arrayProp, Ref } from 'typegoose';
import { getModelForClassFunction } from '../../utils/mongodb';

import * as mongodbUtils from "../../utils/mongodb";


export class Morador extends Typegoose {

    public static graphQLTypes : string = `
        type Morador {
            _id: ID!
            nome: String!
            tipoApartamento: String!
            numAndarApartamento: Int!
            numApartamento: Int!
        }


        input MoradorCreateInput {
            nome: String!
            tipoApartamento: String!
            numAndarApartamento: Int!
            numApartamento: Int!
        }


        input MoradorUpdateInput {
            nome: String!
            tipoApartamento: String!
            numAndarApartamento: Int!
            numApartamento: Int!
        }
    `;


    @prop({ required: true })
    public nome: String;
    
    @prop({ required: true })
    public tipoApartamento: String;

    @prop({ required: true })
    numAndarApartamento: number;

    @prop({ required: true })
    numApartamento: number;    
}

export type MoradorDocument = mongodbUtils.MongooseDocument<Morador>;
export const MoradorModel : mongodbUtils.MongooseModel<MoradorDocument, Morador> = 
    getModelForClassFunction(Morador);

export default {}

