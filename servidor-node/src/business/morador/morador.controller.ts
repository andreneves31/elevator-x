import { MoradorModel, MoradorDocument, Morador } from "./morador.model";
import * as mongodbUtils from "../../utils/mongodb";
import { getVariableValues } from "graphql/execution/values";
import { ObjectId } from "bson";
import { logError } from "../../utils/log";
import { ErroDesconhecidoException } from "../../utils/global";


const entityName: string = 'Morador';

export default class MoradorController {
    public static graphQLQuerys: string = `
        moradores(offset: Int, limit: Int) : [ Morador! ]!
        morador(id : ID!) : Morador
    `;

    public static graphQLMutations: string = `
        createMorador(input: MoradorCreateInput!): Morador
        updateMorador(id: ID!, input: MoradorUpdateInput!): Morador
        deleteMorador(id: ID!): Morador
    `;


    public static queryResolvers = ['moradores', 'morador'];

    public static mutationResolvers = [
        'createMorador',
        'updateMorador',
        'deleteMorador'
    ];

    public static fieldLevelResolvers = {
        //Empresa: ['produtos']
    }



    /**
     * empresasResolver: Retorna uma lista de empresas
     */
    public static moradoresResolver(parent, { offset = 0, limit = 10 }, context, info) {
        const query =
            MoradorModel
            .find()
            .skip(offset)
            .limit(limit)
            .sort({ nome: 'asc' })
            .lean();
            

        return query;
    }

    /**
     * empresaResolver: Retorna um empresa específica
     */
    public static async moradorResolver(parent, args, context, info) {
        await mongodbUtils.verifyDocumentExists(MoradorModel, args.id, entityName);

        // TODO: Filtrar campos requeridos
        const query = 
            MoradorModel
            .findById(args.id)
            .lean();

        return query;
    }

    public static createMoradorResolver(parent, args, context, info) {
        // TODO: Filtrar campos requeridos
        return MoradorModel.create(args.input);
    }

    public static async updateMoradorResolver(parent, args, context, info): Promise<any> {
        return await mongodbUtils.updateDocument(MoradorModel, args.id, args.input, entityName);
    }

    public static async deleteMoradorResolver(parent, args, context, info) {
        return await mongodbUtils.deleteDocument(MoradorModel, args.id, entityName);
    }

}

