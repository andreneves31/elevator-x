import { makeExecutableSchema } from 'graphql-tools';
import { GraphQLSchema } from 'graphql';

import { Morador } from '../business/morador/morador.model';
import MoradorController from '../business/morador/morador.controller';


class Schema {
    controllers = [
        MoradorController
    ];

    models = [
        Morador
    ];

    schemaDefinition  = `
        type Schema {
            query: Query
            mutation: Mutation
        }
    `;

    globalTypes = `
        type Error {
            name: String!
            message: String!
        }
    `
    
    
    queries : string;
    mutations : string;
    
    
    types : string[] = [] ;
    resolvers : any = {};
    
    schema : GraphQLSchema;


    constructor() {
        
        // Define as queries, mutations e resolvers
        this.buildControllers();

        // Define os tipos
        this.buildModels();

        

        // Processa o esquema
        this.makeSchema();

    }

    private buildControllers() {
        const queries = [];
        queries.push('type Query {');

        const mutations = [];
        mutations.push('type Mutation {');


        this.resolvers.Query = {};
        this.resolvers.Mutation = {};


        this.controllers.forEach((controller) => {
            queries.push('\t');
            queries.push(controller.graphQLQuerys);
            queries.push('\n');


            mutations.push('\t');
            mutations.push(controller.graphQLMutations);
            mutations.push('\n');


            // Resolvers de queries
            controller.queryResolvers.forEach((resolver) => {
                this.resolvers.Query[resolver] = controller[ resolver + 'Resolver' ]
            });

            // Resolvers de mutations
            controller.mutationResolvers.forEach((resolver) => {
                this.resolvers.Mutation[resolver] = controller[ resolver + 'Resolver' ]
            });

            
            Object.entries(controller.fieldLevelResolvers).forEach(( [key, value] : [string, string[]]) => {
                let obj = this.resolvers[key];
                
                if(obj == null) {
                    this.resolvers[key] = {};
                    obj = this.resolvers[key];
                }

                value.forEach((resolver) => {
                    obj[resolver] = controller[ resolver + 'Resolver'];
                });
            })
        });

        console.log(this.resolvers);


        queries.push('}');

        mutations.push('}');

        this.queries = queries.join('');
        this.mutations = mutations.join('');
    }


    private buildModels() {
        this.models.forEach((model) => {
            this.types.push(model.graphQLTypes);
        });
    }


    private makeSchema() {
        this.types.push(this.schemaDefinition);
        this.types.push(this.queries);
        this.types.push(this.mutations);
        this.types.push(this.globalTypes);


        this.schema = makeExecutableSchema({
            typeDefs: this.types,
            resolvers: this.resolvers
        });
    }


}





export default new Schema().schema;

