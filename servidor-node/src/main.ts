import * as cluster from 'cluster';
import { CpuInfo, cpus } from 'os';

class Main {

    private cpus: CpuInfo[];

    constructor() {
        this.cpus = cpus();
        this.init();
    }

    init(): void {
        if (cluster.isMaster) {

            this.cpus.forEach(() => {
                const w = cluster.fork();
                w.on('message', this.onWorkerMsg.bind(this));
            })

            cluster.on('listening', (worker: cluster.Worker) => {
                console.log('Cluster %d listening', worker.process.pid);                
            });

            cluster.on('disconnect', (worker: cluster.Worker) => {
                console.log('Cluster %d disconnected', worker.process.pid);                
            });

            cluster.on('exit', (worker: cluster.Worker) => {
                console.log('Cluster %d exited', worker.process.pid);
                
                const w = cluster.fork();
                w.on('message', this.onWorkerMsg.bind(this));
            });

            

        } else {
            require('./server/');
        }
    }


    onWorkerMsg(msg) {
        console.log('msg', msg);
    }

}

export default new Main();