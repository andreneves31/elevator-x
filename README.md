# Elevator X

Apresentação de um projeto de elevador predial com novos recursos, com foco em promover acessibilidade para cegos

<br>

# Arquitetura do sistema

* O elevador terá um painel no lado interno e externo. No projeto, será representado por dois celulares / tablets

* O painel será uma página aberta no navegador


<br>

## Servidor da aplicação

 * Para que o raspberry seja o servidor da aplicação, é necessário que os celulares / tablets estejam na mesma rede local. 
    * Isso é possível com configuração automática de um hotspot no raspberr, mas no momento não tenho segurança para confiar que a inicialização do hotspot será feita de forma automática;

    * Outras alternativa é ligar um roteador local, mas ainda assim existe o problema da configuração errada. Pode haver uma configuração incorreta de IP.

 * Talvez seja melhor usar um servidor remoto, na internet, para a aplicação. O navegador deverá conectar-se ao endereço, que receberá os comandos de voz e repassará para a aplicação do raspberry. Configurar a aplicação para inicialização automátia
    * A aplicação será feita em Node, e receberá, via websockets, os comandos do servidor remoto.



<br>

## Decisão sobre servidor

 * Servidor remoto para exibir as páginas do sistema e receber o comando. O raspberry / arduíno irá conectar-se ao servidor remoto para receber os comandos e controlar o elevador


 <br>

# Subprojetos

A programação será dividida em três subprojetos:

 * Front-end: Terá as interfaces gráficas dos painéis do elevador, além das interfaces de cadastros de moradores e outras funções.
 
 * Servidor: Responsável pelo banco de dados dos moradores, pelo recebimento do streaming do comando de voz e do envio dos comandos para controle do elevador.

 * Controle do elevador: Aplicação que será executada no Raspberry PI. Será responsável por redirecionar os comandos para o Arduíno, via porta USB. 

