// Olá
// Deseja ir para onde?

// Ok
// Indo para o primeiro andar


var listening = false;

navigator.mediaDevices.getUserMedia({
    audio: true
}).then(stream => {
    // Handle the incoming audio stream
    const bars = [] // We'll use this later
    const audioContext = new AudioContext();
    const input = audioContext.createMediaStreamSource(stream);
    const analyser = audioContext.createAnalyser();
    const scriptProcessor = audioContext.createScriptProcessor();

    // Some analyser setup
    analyser.smoothingTimeConstant = 0.3;https://blog.sambego.be/creating-an-audio-waveform-from-your-microphone-input/
    analyser.fftSize = 1024;
    
    input.connect(analyser);
    analyser.connect(scriptProcessor);
    scriptProcessor.connect(audioContext.destination);



    
    
    
    const processInput = audioProcessingEvent => {
        const tempArray = new Uint8Array(analyser.frequencyBinCount);
        analyser.getByteFrequencyData(tempArray);
        const volume = getAverageVolume(tempArray);
    
            
        if(volume > 25 && listening == false) {
            listening = true;
            console.log('trigger');
                
                            
            recognition.start();
            console.log('Ready to receive a color command.');
            

            
            setTimeout(function() { recognition.stop() ; console.log('stop') } , 5000);
        }
        // We'll create this later
        //console.log(bars);
    }

    const getAverageVolume = array => {
        const length = array.length;
        let values = 0;
        let i = 0;

        for (; i < length; i++) {
            values += array[i];
        }

        return values / length;
    }

    scriptProcessor.onaudioprocess = processInput;

}, error => {
    // Something went wrong, or the browser does not support getUserMedia
});











var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent

var colors = [ 'aqua' , 'azure' , 'beige', 'bisque', 'black', 'blue', 'brown', 'chocolate', 'coral', 'crimson', 'cyan', 'fuchsia', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green', 'indigo', 'ivory', 'khaki', 'lavender', 'lime', 'linen', 'magenta', 'maroon', 'moccasin', 'navy', 'olive', 'orange', 'orchid', 'peru', 'pink', 'plum', 'purple', 'red', 'salmon', 'sienna', 'silver', 'snow', 'tan', 'teal', 'thistle', 'tomato', 'turquoise', 'violet', 'white', 'yellow'];
var grammar = '#JSGF V1.0; grammar colors; public <color> = ' + colors.join(' | ') + ' ;'

var recognition = new SpeechRecognition();
var speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
//recognition.continuous = false;
recognition.lang = 'pt-BR';
console.log('language', recognition.lang);
recognition.interimResults = false;
recognition.maxAlternatives = 1;

var diagnostic = document.querySelector('.output');
var bg = document.querySelector('html');
var hints = document.querySelector('.hints');

var colorHTML= '';
colors.forEach(function(v, i, a){
  console.log(v, i);
  colorHTML += '<span style="background-color:' + v + ';"> ' + v + ' </span>';
});
hints.innerHTML = 'Tap/click then say a color to change the background color of the app. Try '+ colorHTML + '.';

diagnostic.textContext = 'Waiting for trigger';


document.body.onclick = function() {
  recognition.start();
  console.log('Ready to receive a color command.');
  

  
  setTimeout(function() { recognition.stop() ; console.log('stop') } , 5000);
}

recognition.onresult = function(event) {
    listening = false;
  // The SpeechRecognitionEvent results property returns a SpeechRecognitionResultList object
  // The SpeechRecognitionResultList object contains SpeechRecognitionResult objects.
  // It has a getter so it can be accessed like an array
  // The [last] returns the SpeechRecognitionResult at the last position.
  // Each SpeechRecognitionResult object contains SpeechRecognitionAlternative objects that contain individual results.
  // These also have getters so they can be accessed like arrays.
  // The [0] returns the SpeechRecognitionAlternative at position 0.
  // We then return the transcript property of the SpeechRecognitionAlternative object

  var last = event.results.length - 1;
  var color = event.results[last][0].transcript;
  console.log(color);

  diagnostic.textContent = 'Result received: ' + color + '.';
  bg.style.backgroundColor = color;
  console.log('Confidence: ' + event.results[0][0].confidence);
}

recognition.onspeechend = function() {
  recognition.stop();
}

recognition.onnomatch = function(event) {
  diagnostic.textContent = "I didn't recognise that color.";
}

recognition.onerror = function(event) {
  diagnostic.textContent = 'Error occurred in recognition: ' + event.error;
}
